
package ClassesATester;

public class NormalisationClient {
	
	public NormalisationClient() {
		
	}
	public static String capitalizeString(String string) {
	 	  char[] chars = string.toLowerCase().toCharArray();
		  boolean found = false;
		  for (int i = 0; i < chars.length; i++) {
		    if (!found && Character.isLetter(chars[i])) {
		      chars[i] = Character.toUpperCase(chars[i]);
		      found = true;
		    } else if (Character.isWhitespace(chars[i]) || chars[i]=='.' || chars[i]=='-') { 
		      found = false;
		    }
		  }
		  return String.valueOf(chars);
		
		}
	public String normaliserPays(String pays) {
		pays = pays.trim();
		char[] char_table = pays.toCharArray();
		char_table[0]=Character.toUpperCase(char_table[0]);
		pays = new String(char_table);
		
		if (pays.equalsIgnoreCase("letzebuerg")) {
			pays = "Luxembourg";
		}
		else if (pays.equalsIgnoreCase("belgium")) {
			pays = "Belgique";
		}
		else if (pays.equalsIgnoreCase("schweiz")) {
			pays = "Suisse";
		}
		
		
		return pays;
	}
	
	public String normaliserVille(String ville) {
		ville = ville.trim();
		char[] char_table = ville.toCharArray();
		char_table[0]=Character.toUpperCase(char_table[0]);
		ville = new String(char_table);
		
		if (ville.indexOf(" l�s ")!=-1) {
			ville = ville.replaceAll(" l�s ","-l�s-");
			
		}
		else if (ville.indexOf(" le ")!=-1) {
			ville = ville.replaceAll(" le ", "-le-");
			
		}
		else if (ville.indexOf(" sous ")!=-1) {
			ville = ville.replaceAll(" sous ", "-sous-");
			
		}
		else if (ville.indexOf(" sur ")!=-1) {
			ville = ville.replaceAll(" sur ", "-sur-");
			
		}
		else if (ville.indexOf(" � ")!=-1) {
			ville = ville.replaceAll(" � ", "-�-");
			
		}
		else if (ville.indexOf(" aux ")!=-1) {
			ville = ville.replaceAll(" aux ", "-aux-");
			
		}
		else if (ville.indexOf("Ste")!=-1) {
			ville = ville.replaceAll("Ste ", "Sainte-");
			
		}
		else if (ville.indexOf("ste")!=-1) {
			ville = ville.replaceAll("ste ", "Sainte-");
			
		}
		else if (ville.indexOf("St")!=-1) {
			ville = ville.replaceAll("St ", "Saint-");	
		}
		else if (ville.indexOf("st")!=-1) {
			ville = ville.replaceAll("st ", "Saint-");	
		}
		else if (ville.indexOf("-l�s-")!=-1) {
			ville = capitalizeString(ville);
		}
		else if (ville.indexOf("-le-")!=-1) {
			ville = capitalizeString(ville);
			
		}
		else if (ville.indexOf("-sous-")!=-1) {
			ville = capitalizeString(ville);
			
		}
		else if (ville.indexOf("-sur-")!=-1) {
			ville = capitalizeString(ville);
			
		}
		else if (ville.indexOf("-�-")!=-1) {
			ville = capitalizeString(ville);
			
		}
		else if (ville.indexOf("-aux-")!=-1) {
			ville = capitalizeString(ville);
			
		}
		else if (ville.indexOf("Sainte-")!=-1) {
			ville = capitalizeString(ville);
			
		}
		else if (ville.indexOf("Saint-")!=-1) {
			ville = capitalizeString(ville);
			
		}
		
		return ville;
	}
	
	public String normalisercodespostaux(String code) {
		code = code.trim();
		if(code.length() ==4) {
			code = "0"+code;
			
		}
		else if (Character.isDigit(code.charAt(0)) == false) {
			code = code.replaceAll(code, code.substring(2, code.length()));//si le premier n'est pas un chiffre, on prend les 4 derniers chiffres. Ne prend en compte que le format Initiale du temps de pays + 1 tiret.
			
		}
		return code;
	}
	public String normaliserVoie(String voie) {
		voie = voie.trim();
		if(voie.indexOf(",")==-1) {
			voie = voie.replaceFirst(" ", ", ");
		}
		else if(voie.indexOf("boulevard")!=-1) {
			voie = voie;
		}
		else if(voie.indexOf("boul.")!=-1) {
			voie = voie.replaceAll("boul.", "boulevard");
		}
		else if(voie.indexOf("boul")!=-1) {
			voie = voie.replaceAll("boul", "boulevard");
		}
		
		else if(voie.indexOf("bd")!=-1) {
			voie = voie.replaceAll("bd", "boulevard");
		}
		else if(voie.indexOf("avenue")!=-1) {
			voie = voie;
		}
		else if(voie.indexOf("av.")!=-1) {
			voie = voie.replaceAll("av.", "avenue");
		}
		else if(voie.indexOf("faubourd")!=-1) {
			voie = voie;
		}
		else if(voie.indexOf("faub.")!=-1) {
			voie = voie.replaceAll("faub.", "faubourg");
		}
		else if(voie.indexOf("fg")!=-1) {
			voie = voie.replaceAll("fg", "faubourg");
		}
		else if(voie.indexOf("place")!=-1) {
			voie = voie;
		}
		else if(voie.indexOf("pl.")!=-1) {
			voie = voie.replaceAll("pl.", "place");
		}
		
		
		
		return voie;
	}
	

}


