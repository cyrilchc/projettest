package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import ClassesATester.NormalisationClient;

public class NormalisationClientTest {

	@Test
	public void testPaysEspaces() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Luxembourg", a.normaliserPays("   Luxembourg"));

	}
	@Test
	public void testPaysBelgique() {
	
		NormalisationClient a = new NormalisationClient();
		assertEquals("Belgique", a.normaliserPays("belgium"));
	}
	@Test
	public void testPaysSuisse() {
		NormalisationClient b = new NormalisationClient();
		assertEquals("Suisse", b.normaliserPays("schweiz"));
	}	
	@Test
	public void testPaysLux() {
		NormalisationClient c = new NormalisationClient();
		assertEquals("Luxembourg", c.normaliserPays("letzebuerg"));
	}
	@Test
	public void testPaysSansMaj() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("France", a.normaliserPays("france"));
	}
	
	@Test
	public void testPaysAvecMaj() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("France", a.normaliserPays("France"));
	}
	
	
	
	@Test
	public void testNomsVilleNormal() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Saizerais", a.normaliserVille("Saizerais"));
	}
	@Test
	public void testNomsVilleEspace() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Saizerais", a.normaliserVille("     saizerais"));
	}
	@Test
	public void testNomsVilleMaj() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Saizerais", a.normaliserVille("saizerais"));
	}
	@Test
	public void testNomsVillePrepl�s() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Blenod-l�s-Pont", a.normaliserVille("Blenod l�s Pont"));
	}
	@Test
	public void testNomsVillePrepLe() {
		NormalisationClient b = new NormalisationClient();
		assertEquals("Blenod-le-Pont", b.normaliserVille("Blenod le Pont"));
	}
	@Test
	public void testNomsVillePrepSous() {
		NormalisationClient c = new NormalisationClient();
		assertEquals("Blenod-sous-Lepont", c.normaliserVille("Blenod sous Lepont"));
	}
	@Test
	public void testNomsVillePrepSur() {
		NormalisationClient d = new NormalisationClient();
		assertEquals("Pagny-sur-Moselle", d.normaliserVille("Pagny sur Moselle"));
	}
	@Test
	public void testNomsVillePrepA() {
		NormalisationClient e = new NormalisationClient();
		assertEquals("Pont-�-Mousson", e.normaliserVille("Pont � Mousson"));
	}
	@Test
	public void testNomsVillePrepAux() {
		NormalisationClient f = new NormalisationClient();
		assertEquals("Bouxi�re-aux-Dames", f.normaliserVille("Bouxi�re aux Dames"));
	}
	@Test
	public void testNomsVilleAbreviationsSt() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Saint-Nazaire", a.normaliserVille("St Nazaire"));
	}
	@Test
	public void testNomsVilleAbreviationsst() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Saint-Nazaire", a.normaliserVille("st Nazaire"));
	}
	@Test
	public void testNomsVilleAbreviationsSte() {
		NormalisationClient b = new NormalisationClient();
		assertEquals("Sainte-Maure", b.normaliserVille("Ste Maure"));
	}
	@Test
	public void testNomsVilleAbreviationsste() {
		NormalisationClient b = new NormalisationClient();
		assertEquals("Sainte-Maure", b.normaliserVille("ste Maure"));
	}
	@Test
	public void testNomsVilleMajusculesNormal() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Blenod-L�s-Pont", a.normaliserVille("Blenod-l�s-Pont"));
	}
	@Test
	public void testNomsVilleMajusculesL�s() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Blenod-L�s-Pont", a.normaliserVille("blenod-l�s-pont"));
	}
	@Test
	public void testNomsVilleMajusculesLe() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Blenod-Le-Pont", a.normaliserVille("blenod-le-pont"));
	}
	@Test
	public void testNomsVilleMajusculesSous() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Blenod-Sous-Lepont", a.normaliserVille("blenod-sous-lepont"));
	}
	@Test
	public void testNomsVilleMajusculesSur() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Pagny-Sur-Moselle", a.normaliserVille("Pagny-sur-moselle"));
	}
	@Test
	public void testNomsVilleMajusculesA() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Pont-�-Mousson", a.normaliserVille("Pont-�-mousson"));
	}
	@Test
	public void testNomsVilleMajusculesAux() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Bouxi�res-Aux-Dames", a.normaliserVille("Bouxi�res-aux-dames"));
	}
	@Test
	public void testNomsVilleMajusculesSte() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Sainte-Maure", a.normaliserVille("sainte-maure"));
	}
	@Test
	public void testNomsVilleMajusculesSt() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("Saint-Nazaire", a.normaliserVille("saint-nazaire"));
	}
	
	
	@Test
	public void testCodePoNumeroErreur() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("05438", a.normalisercodespostaux("5438"));
	}
	@Test
	public void testCodePoNumeroNormal() {
		NormalisationClient b = new NormalisationClient();
		assertEquals("57000", b.normalisercodespostaux("57000"));
	}
	@Test
	public void testCodePoespacesl() {
		NormalisationClient b = new NormalisationClient();
		assertEquals("57000", b.normalisercodespostaux("    57000"));
	}
	
	
	@Test
	public void testCodeEtranger() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("2270", a.normalisercodespostaux("L-2270"));
		

	}
	@Test
	public void testVoieAbreviationEspaces() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("3, boulevard de Verdun", a.normaliserVoie("      3, boulevard de Verdun"));
	}
	@Test
	public void testVoieAbreviationBoulevard() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("3, boulevard de Verdun", a.normaliserVoie("3, boulevard de Verdun"));
	}
	@Test
	public void testVoieAbreviationBoul() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("3, boulevard de Verdun", a.normaliserVoie("3, boul de Verdun"));
	}
	@Test
	public void testVoieAbreviationBoul2() {
		NormalisationClient b = new NormalisationClient();
		assertEquals("3, boulevard de Verdun", b.normaliserVoie("3, boul. de Verdun"));
	}	
	@Test
	public void testVoieAbreviationBd() {
		NormalisationClient c = new NormalisationClient();
		assertEquals("3, boulevard de Verdun", c.normaliserVoie("3, bd de Verdun"));
	}	
	@Test
	public void testVoieAbreviationAvenue() {
		NormalisationClient d = new NormalisationClient();
		assertEquals("3, avenue de l'Europe", d.normaliserVoie("3, av. de l'Europe"));
	}
	@Test
	public void testVoieAbreviationAv() {
		NormalisationClient d = new NormalisationClient();
		assertEquals("3, avenue de l'Europe", d.normaliserVoie("3, av. de l'Europe"));
	}
	@Test
	public void testVoieAbreviationFaub() {
		NormalisationClient e = new NormalisationClient();
		assertEquals("3, rue du faubourg de saverne", e.normaliserVoie("3, rue du faub. de saverne"));
	}
	@Test
	public void testVoieAbreviationFg() {
		NormalisationClient f = new NormalisationClient();
		assertEquals("3, rue du faubourg de saverne", f.normaliserVoie("3, rue du fg de saverne"));
	}
	@Test
	public void testVoieAbreviationPl() {
		NormalisationClient g = new NormalisationClient();
		assertEquals("3, place du G�n�ral Leclerc", g.normaliserVoie("3, pl. du G�n�ral Leclerc"));
	}	
	@Test
	public void testVoieVirgule() {
		NormalisationClient a = new NormalisationClient();
		assertEquals("3, boulevard de Verdun", a.normaliserVoie("3 boulevard de Verdun"));
	
	}
	
	

}

